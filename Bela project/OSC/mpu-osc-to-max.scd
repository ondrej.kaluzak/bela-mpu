
s = Server.default;
s.options.numAnalogInChannels = 8;
s.options.numAnalogOutChannels = 8;
s.options.numDigitalChannels = 16;
s.options.blockSize = 16;
s.options.numInputBusChannels = 2;
s.options.numOutputBusChannels = 2;

// TREBA NASTAVIT IP ADRESU LOCAL HOSTU KDE BEZI MAX
b = NetAddr.new("192.168.1.104", 7400);

s.waitForBoot({
	MPU.calibrateAccelGyro(s);
	SynthDef(\osctest,{
		var mpu = MPU.kr();
		SendReply.kr(Impulse.kr(10), "/mpuRaw", mpu);
	}).add;

	s.sync;

	OSCdef(\mpuOSCoutput, {arg msg;  
			b.sendMsg("MPU", "% % % % % % % % % % % %".format(msg[3], msg[4], msg[5], msg[6], msg[7], msg[8], msg[9], msg[10], msg[11], msg[12], msg[13], msg[14], msg[15])); 
			
	}, "/mpuRaw");

	Synth.new(\osctest);
});

ServerQuit.add({ 0.exit }); // quit if the button is pressed
