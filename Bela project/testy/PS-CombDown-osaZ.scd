
/*
ZNIE SUPER SPOLU S TGRAINS OSA Z
*/

(
SynthDef.new(\combDown,{
  var precision = 0.1;
  var offset = 6;
  var combed = 0;
  var lag = 0.1;
  
	var orien = MPU.orientationKr();
	var gyro = MPU.gyroKr();

  var ox = Lag.kr(orien.at(0).round(precision).linlin(-85,70,0,2), lag);
  var oz = Lag.kr(orien.at(2).round(1).linlin(1, 360, 0.1, 3), lag);
  var gx = Lag.kr(gyro.at(0).round(precision).abs/200, lag);
  var gy = Lag.kr(gyro.at(1).round(precision).abs/230, lag);
  var gz = Lag.kr(gyro.at(2).round(precision).abs/230, lag);

  var f1, f2, f3, sig, combF, selection, pitchShift;
  var sigIn = SoundIn.ar(0);
  var freq, hasFreq, sim;

  # freq, hasFreq = Pitch.kr(sigIn, ampThreshold: 0.1, median: 1, downSample:3);
	f1 = Lag.kr(freq, gz);

	// pitchShift = PitchShift.ar(sigIn, windowSize: 0.1, pitchRatio: 2-ox, pitchDispersion: gy, timeDispersion: gx, mul: 1.0, add: 0.0);
	pitchShift = PitchShift.ar(sigIn, windowSize: 0.1, pitchRatio: oz, pitchDispersion: gy, timeDispersion: ox, mul: 1.0, add: 0.0);

  sig = sigIn;
   combF = (f1*0.1).reciprocal;


    combed = CombL.ar(pitchShift, 0.2, [combF, combF*1.01], 1);


  // sig = Splay.ar(pitchShift+combed);
	//sig = Splay.ar(pitchShift);
	sig = Mix.ar(pitchShift+combed)*0.3;
  Out.ar(0,sig!2);

  }).add;
)

~combDownSynth = Synth(\combDown, [\speed, 3]);
~combDownSynth.free;