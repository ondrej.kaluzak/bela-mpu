MPU : MultiOutUGen {
    /*
    Channels:
    0: Accel
    1: Gyro
    2: Mag
    3: Orientation (pitch, roll, yaw)
    4: Pitch
    5: Roll
    6: Yaw
    */
   *init { arg argNumChannels;// ... theInputs;
		//inputs = theInputs;
	^this.multiNew('control', 0).initOutputs(12, 'control');

	}

    *kr {
        arg channel = 0, channels = 1, mul = 1.0, add = 0; 

        ^this.multiNew('control', channel).initOutputs(channels, 'control').madd(mul, add)
    }

    *accelKr {
        arg mul = 1.0, add = 0;
        ^this.kr(0, 3, mul, add);
    }

    *gyroKr {
        arg mul = 1.0, add = 0;
        ^this.kr(1, 3, mul, add);
    }

    *magKr {
        arg mul = 1.0, add = 0;
        ^this.kr(2, 3, mul, add);
    }

    *orientationKr {
        arg mul = 1.0, add = 0;
        ^this.kr(3, 3, mul, add);
    }

    *pitchKr{
        arg mul = 1.0, add = 0;
        ^this.kr(4, 1, mul, add);
    }

    *rollKr {
        arg mul = 1.0, add = 0;
        ^this.kr(5, 1,  mul, add);
    }

    *yawKr{
        arg mul = 1.0, add = 0;
        ^this.kr(6, 1, mul, add);
    }

    *calibrateAccelGyro { |server|
        (server ? Server.default).sendMsg(\cmd, \mpuCmd, 1);
    }

    *calibrateMag { |server|
        (server ? Server.default).sendMsg(\cmd, \mpuCmd, 2);
    }

    *saveCalibration { |path, server|
        path = path ?? { Platform.userConfigDir +/+ "mpuCalibration.bin" };
        //No error checking here -- server might be remote
        (server ? Server.default).sendMsg(\cmd, \mpuCmd, 3, path);
    }

    *loadCalibration { |path, server|
        path = path ?? { Platform.userConfigDir +/+ "mpuCalibration.bin" };
        //No error checking here -- server might be remote
        (server ? Server.default).sendMsg(\cmd, \mpuCmd, 4, path);
    }

}
