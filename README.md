# Repozitár k projektu Cyber Trio (špecifický výskum v rámci JAMU)
 Repozitár obsahuje:
 - upravené ovládače pre komunikáciu so senzorom MPU9250 cez protokol I2C na platforme Bela
 - viacero patchov napísanych v Supercollideri pre modifikáciu živého audio signálu využívajúci dáta zo senzoru
 - logiku pre prepínanie presetov za pomoci haptických senzorov (Trill Craft)
 - remote rozhranie pre pohodlnejší vývoj ďalších algoritmov
 - Max/MSP patch, pre pomocnú vizualizáciu dát zo senzoru MPU9250

_(Poznámka: repozitár sa bude v najbližších dňoch ešte aktualizovať, triediť a sprehľadňovať, aby ku všektému bol rozumný popis a odkaz)_


# Inštalácia

# A). Postup bez zostavovania, použitie hotového buildu:
1. stiahnem si z repozitaru 2 subory:
    - [binarny MPU.so](https://gitlab.com/ondrej.kaluzak/bela-mpu/-/blob/main/BUILD/fix2/MPU.so)
    - [modifikovana definicia uGenu](https://gitlab.com/ondrej.kaluzak/bela-mpu/-/blob/main/BUILD/fix2/MPU9250.sc)

2. pripojim sa cez terminal do bely `ssh root@bela.local`

3. vytvorim zlozku MPU v extensions
    - `mkdir /root/.local/share/SuperCollider/Extensions/MPU`

4. skopirujem tam oba subory:
    -  otvorim si **novy** (dolezite je nepouzivat terminal ktory je pripojeny k bele) terminal **v zlozke**, kde som si tie dva subory stiahol 
    - postupne zadam:
    - `scp MPU.so root@bela.local:/root/.local/share/SuperCollider/Extensions/MPU`
    - `scp MPU9250.sc root@bela.local:/root/.local/share/SuperCollider/Extensions/MPU`

5. Hotovo!

6. Skus teraz napr tento testovaci projekt [projekt](https://gitlab.com/ondrej.kaluzak/bela-mpu/-/blob/main/Bela%20project/testy/test_mpu.scd)
    Po spustení by sa mali do konzoly začať vypisováť zvolené dáta zo senzoru.
    

# B). Postup na zostavenie zo zdrojovych kodov v Bele:

1. stiahnut si original zdrojaky driveru MPU, stiahnut upravene zdrojaky:
    - originalne: https://gitlab.com/ondrej.kaluzak/bela-mpu/-/tree/main/ORG
    - upravene: https://gitlab.com/ondrej.kaluzak/bela-mpu/-/tree/main/EDIT/plugin

2. rozbalit original zdrojaky, nahradit v nich celu zlozku `plugin`, zlozkou z upravenych zdrojakov

3. takto upravene zdrojaky znova zabalit, ideal cez terminal s pouzitim tarball
    - cize otvorim si terminal v zlozke o "poschodie vyssie" kde mam zdrojaky uz so zmenenou zlozkou plugin
    - zadam `tar -czvf bela-mpu-src.tar.gz <meno zlozky kde su zdrojaky>`

4. tento balik teraz treba nakopirovat do Bely a tam zase rozbalit:
    - skopirovanie `scp <cesta k zdrojakom/bela-mpu-src.tar.gz> root@bela.local/~/MPU` (zlozky MPU bude mozno najprv treba vytvorit v Bele)

5. skopirovat zdrojaky supercollideru do bely
    - link SC zdrojakov: https://github.com/supercollider/supercollider/archive/refs/tags/Version-3.11.2.tar.gz
    - premenovat subor na `supercollider-3.11.2.tar.gz`
    - otvorit terminal v zlozke so zdrojakmi k SC
    - `scp supercollider-3.11.2.tar.gz root@bela.local/~/SC` (znova, ak kopirovanie zlyha treba najprv vytvorit zlozku SC)

6. Rozbalenie zdrojakov Bely MPU a SC
    - teraz v Bele treba vsetko zase rozbalit
    - takze cez terminal sa pripojit do bely `ssh root@bela.local`
    - navigovat sa do zlozky s prvymi zdrojakmi, cize asi `cd ~/MPU`
    - zadat `tar -xf bela-mpu-src.tar.gz`
    - navigovat sa do zlozky so zdrojakmi k SC, cize asi `cd ~/SC`
    - zadat `tar -xf supercollider-3.11.2.tar.gz`

7. Taaak a teraz by malo byt pripravene vsetko na build, ktory bude prebiehat priamo na Bele
    - teraz pojdeme podla postupu aky tam mal tipek povodne s jednou zmenou, ze pridame do cmake cestu k supercollider zdrojakom
    - navigujem sa do zlozky s MPU zdrojakmi, cize asi `cd ~/MPU/bela-mpu-src`, ta cesta sa moze mierne lisit, ale skratka treba byt v zlozke, kde su zlozky `app`, `libs`, `plugin`, `scripts`, a subory `CMakeLists.txt` a nejake dalsie..
    - zadat postupne: 
    - `mkdir build`
    - `cd build`
    - `cmake -DSC_PATH=~/SC/supercollider-3.11.2/ -DCMAKE_BUILD_TYPE=release ../`
    - `make`
    - co je velmi dolezite, aby cesta `~/SC/supercollider-3.11.2/` existovala, cize ked tak skontrolovat, ze v tejto zlozke su ozaj zdrojaky supercollideru, a nie su vnorene este do nejakej dalsej (cize by tam malo byt uz kopec suborov a zloziek)

8. po tomto kroku by sa mal uspesne zostavit supercollidracky binarny subor MPU.so

9. ten teraz treba spolu s Ugenom skopirovat do zlozky, kde to Supercollider najde, cize do zlozky `/root/.local/share/SuperCollider/Extensions/(MPU)`
napr prikazom `cp ~/MPU/bela-mpu-src/build/plugin/MPU.so` `/root/.local/share/SuperCollider/Extensions/MPU` (zlozku MPU v Extensions bude zrejme treba najprv vytvorit)
a finalne nakopirovat Ugen
`cp ~/MPU/bela-mpu-src/plugin/MPU9250.sc` `/root/.local/share/SuperCollider/Extensions/MPU`

10. Olala, teraz ked sa spusti Supercollider projekt a budete sa odkazovat na MPU Ugen, tak to bude fungovat! :D 
Skuste si stiahnut a uploadnot do Bely(alebo proste len ctr+c ctrl +v ako text to skopcit do nejakeho ineho) tento projekt `https://gitlab.com/ondrej.kaluzak/bela-mpu/-/tree/main/Bela%20project`

# Ďalšie poznámky, odkazy:

# 3. Skratky

Test senzoru:
- Akcelertometer, gyro, magnetometer, teplota
`ssh root@bela.local`
`cd ~/Bela/libraries/MPU/sc-mpu9250 && sudo ./build/apps/debug/debug`

- Orientacia (pitch, roll, yaw)
`ssh root@bela.local`
`cd ~/Bela/libraries/MPU/sc-mpu9250 && sudo ./build/apps/ahrs_debug/ahrs_debug`


# 4. Odkazy

- výpočet uhlov zo surových dát:
https://www.youtube.com/watch?v=LkV-uXjzXKE&ab_channel=SirReal
https://www.youtube.com/watch?v=YdQeGDgJY-4
- orgiginal repozitar ovladacov
https://github.com/jpburstrom/sc-mpu9250
- ako nainstalovat a zostavit dalsie extensions do SC
http://supercollider.github.io/sc3-plugins/
https://github.com/supercollider/sc3-plugins/tree/5342a4ae8842c7361517001866e986c437f50648

- moja adresa zdrojakov mpu 
~/Bela/libraries/MPU/sc-mpu9250/build/plugin

# 5. Supercollider sikovne tutorialy

- GrainBuff - do detailov
https://www.youtube.com/watch?v=WBqAM_94TW4
https://www.youtube.com/watch?v=MnD8stNB5tE

- GrainBuff a TGrains
https://www.youtube.com/watch?v=C9SeVpiyGmI&ab_channel=EliFieldsteel

- Vlastne classy a metody
https://www.youtube.com/watch?v=9gs0WPnxyak&t=611s&ab_channel=EliFieldsteel
https://scsynth.org/t/loading-external-scd-files/576/2

- basic Delay, looper, buffs
https://www.youtube.com/watch?v=3NgCFsmGQgM&t=1204s

- Delays p.1
https://www.youtube.com/watch?v=VjUcklVHPNk&t=3492s

- Delays p.2 - Comb delays, delay based harmonizer, delay reverb
https://youtu.be/eEyYFt3sIWs?t=3439 - dobry efekt, pitch shift down ale s pomalou am modulaciiu
https://www.youtube.com/watch?v=eEyYFt3sIWs&ab_channel=EliFieldsteel


- zaujimave FFT uGens
freez PV_MagFreeze, PV_BinShift, PV_BinScramble, PV_MagShift (posledny v emxaploch, simulace sepotu)








